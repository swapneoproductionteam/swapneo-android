package com.app.swapneo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.swapneo.util.CameraPreview;
import com.flurgle.camerakit.CameraKit;
import com.flurgle.camerakit.CameraListener;
import com.flurgle.camerakit.CameraView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class FragmentUpload extends FragmentBase
{
    private CameraView camera;

    private File file;

    public static FragmentUpload newInstance()
    {
        FragmentUpload fragment = new FragmentUpload();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_upload, null);

        initData();

        initUi();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        camera.start();
    }

    @Override
    public void onPause() {
        camera.stop();
        super.onPause();
    }

    protected void initData()
    {
        super.initData();

        file = new File(getActivity().getExternalCacheDir(), "IMG-"+System.currentTimeMillis()+".jpg");
    }

    private void initUi()
    {
        ((TextView) view.findViewById(R.id.tvUploadMsg)).setTypeface(appData.getFontRegular());

        camera = (CameraView) view.findViewById(R.id.camera);

        /*camera.addOnLayoutChangeListener(new View.OnLayoutChangeListener()
        {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                mCameraWidth = right - left;
                mCameraHeight = bottom - top;

                width.setText(String.valueOf(mCameraWidth));
                height.setText(String.valueOf(mCameraHeight));

                camera.removeOnLayoutChangeListener(this);
            }
        });*/

        view.findViewById(R.id.ivCapturePhoto).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Animation animationScaleUp = AnimationUtils.loadAnimation(getActivity(), R.anim.pop_in);
                Animation animationScaleDown = AnimationUtils.loadAnimation(getActivity(), R.anim.pop_out);

                AnimationSet growShrink = new AnimationSet(true);
                growShrink.addAnimation(animationScaleUp);
                growShrink.addAnimation(animationScaleDown);
                view.findViewById(R.id.ivCapturePhoto).startAnimation(growShrink);


                captureImage();
            }
        });

        view.findViewById(R.id.ivFlash).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                switch (camera.toggleFlash()) {
                    case CameraKit.Constants.FLASH_ON:
                        Toast.makeText(getActivity(), "Flash on!", Toast.LENGTH_SHORT).show();
                        break;

                    case CameraKit.Constants.FLASH_OFF:
                        Toast.makeText(getActivity(), "Flash off!", Toast.LENGTH_SHORT).show();
                        break;

                    case CameraKit.Constants.FLASH_AUTO:
                        Toast.makeText(getActivity(), "Flash auto!", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        view.findViewById(R.id.ivFlipCamera).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                switch (camera.toggleFacing()) {
                    case CameraKit.Constants.FACING_BACK:
                        Toast.makeText(getActivity(), "Switched to back camera!", Toast.LENGTH_SHORT).show();
                        break;

                    case CameraKit.Constants.FACING_FRONT:
                        Toast.makeText(getActivity(), "Switched to front camera!", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        view.findViewById(R.id.ivGallery).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

            }
        });
    }

    private void captureImage()
    {
        camera.setCameraListener(new CameraListener()
        {
            @Override
            public void onPictureTaken(byte[] jpeg)
            {
                super.onPictureTaken(jpeg);
                long callbackTime = System.currentTimeMillis();
                Bitmap bitmap = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length);

                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        camera.captureImage();
    }
}
