package com.app.swapneo;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.swapneo.util.Validator;

public class ActivityForgotPassword extends ActivityBase
{
    private Button btnSend;

    private EditText etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);

        initData();

        initUi();
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        ((TextView) findViewById(R.id.tvForgotPass)).setTypeface(appData.getFontMedium());

        ((TextView) findViewById(R.id.tvForgotPassMsg)).setTypeface(appData.getFontRegular());

        ((TextInputLayout) findViewById(R.id.tlEmail)).setTypeface(appData.getFontRegular());

        etEmail = (EditText) findViewById(R.id.etEmail);
        etEmail.setTypeface(appData.getFontRegular());

        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setTypeface(appData.getFontRegular());

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                if(!validated())
                    return;
            }
        });
    }

    private boolean validated()
    {
        if (!Validator.validateNameNotNull(etEmail.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_email));

            return false;
        }

        if (!Validator.validateEmail(etEmail.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_valid_email));

            return false;
        }

        return true;
    }
}
