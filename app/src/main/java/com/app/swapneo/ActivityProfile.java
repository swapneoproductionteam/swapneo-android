package com.app.swapneo;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.swapneo.util.Validator;
import com.github.siyamed.shapeimageview.CircularImageView;

public class ActivityProfile extends ActivityBase
{
    private Toolbar toolbar;

    private CircularImageView ivImage;

    private EditText etFullName, etEmail, etMobile;

    private Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile);

        initData();

        initUi();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        overridePendingTransition(R.anim.slide_in_from_left_to_right, R.anim.slide_out_from_left_to_right);

        finish();
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.icon_back_left);

        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        ((TextView) findViewById(R.id.tvTitle)).setTypeface(appData.getFontMedium());

        ivImage = (CircularImageView) findViewById(R.id.ivImage);

        ivImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        etFullName = (EditText) findViewById(R.id.etFullName);
        etFullName.setTypeface(appData.getFontRegular());

        etEmail = (EditText) findViewById(R.id.etEmail);
        etEmail.setTypeface(appData.getFontRegular());

        etMobile = (EditText) findViewById(R.id.etMobile);
        etMobile.setTypeface(appData.getFontRegular());

        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnUpdate.setTypeface(appData.getFontRegular());

        btnUpdate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideSoftKeyboard();

                if(!validated())
                    return;

                onBackPressed();
            }
        });
    }

    private boolean validated()
    {
        if (!Validator.validateNameNotNull(etFullName.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_fullname));

            return false;
        }

        if (!Validator.validateNameNotNull(etEmail.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_email));

            return false;
        }

        if (!Validator.validateEmail(etEmail.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_valid_email));

            return false;
        }

        if (!Validator.validateNameNotNull(etMobile.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_mobile));

            return false;
        }

        return true;
    }
}
