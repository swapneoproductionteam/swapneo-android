package com.app.swapneo;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.RoundedImageView;

public class ActivityNotification extends ActivityBase
{
    private Toolbar toolbar;

    private RecyclerView rvNotification;

    private AdapterNotification adpNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notification);

        initData();

        initUi();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        overridePendingTransition(R.anim.slide_in_from_left_to_right, R.anim.slide_out_from_left_to_right);

        finish();
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.icon_back_left);

        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        ((TextView) findViewById(R.id.tvTitle)).setTypeface(appData.getFontMedium());

        rvNotification = (RecyclerView) findViewById(R.id.rvNotification);
        rvNotification.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        adpNotification = new AdapterNotification();
        rvNotification.setAdapter(adpNotification);
    }

    public class AdapterNotification extends RecyclerView.Adapter<AdapterNotification.MyViewHolder>
    {
        public class MyViewHolder extends RecyclerView.ViewHolder
        {
            private RoundedImageView ivImage;
            private TextView tvNotification, tvPoints;

            public MyViewHolder(View view)
            {
                super(view);

                ivImage = (RoundedImageView) view.findViewById(R.id.ivImage);

                tvNotification = (TextView) view.findViewById(R.id.tvNotification);
                tvNotification.setTypeface(appData.getFontRegular());

                tvPoints = (TextView) view.findViewById(R.id.tvPoints);
                tvPoints.setTypeface(appData.getFontRegular());
            }
        }

        public AdapterNotification()
        {
            //this.moviesList = moviesList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position)
        {
            /*if(alImages.get(position).getImage().equals("ADD_NEW_IMAGE"))
                holder.ivImage.setImageDrawable(getResources().getDrawable(R.drawable.icon_add_image));
            else
            {
                Picasso.with(getApplicationContext()).load(alImages.get(position).getImage()).placeholder(R.drawable.icon_no_image).into(holder.ivImage);
            }*/
        }

        @Override
        public int getItemCount()
        {
            return 25;
        }
    }
}
