package com.app.swapneo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentChat extends FragmentBase
{
    public static FragmentChat newInstance()
    {
        FragmentChat fragment = new FragmentChat();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_chat, null);

        initData();

        initUi();

        return view;
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {

    }
}
