package com.app.swapneo;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.swapneo.util.Validator;
import com.github.siyamed.shapeimageview.CircularImageView;

public class ActivitySignup extends ActivityBase
{
    private EditText etUsername, etEmail, etPassword;

    private Button btnRegister;

    private TextView tvTerms, tvPrivacy;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);

        initData();

        initUi();
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        ((TextView) findViewById(R.id.tvSignUp)).setTypeface(appData.getFontRegular());

        ((TextInputLayout) findViewById(R.id.tlUsername)).setTypeface(appData.getFontRegular());
        ((TextInputLayout) findViewById(R.id.tlEmail)).setTypeface(appData.getFontRegular());
        ((TextInputLayout) findViewById(R.id.tlPassword)).setTypeface(appData.getFontRegular());

        ((TextView) findViewById(R.id.tvBySigning)).setTypeface(appData.getFontRegular());

        ((TextView) findViewById(R.id.tvAnd)).setTypeface(appData.getFontRegular());

        etUsername = (EditText) findViewById(R.id.etUsername);
        etUsername.setTypeface(appData.getFontRegular());

        etEmail = (EditText) findViewById(R.id.etEmail);
        etEmail.setTypeface(appData.getFontRegular());

        etPassword = (EditText) findViewById(R.id.etPassword);
        etPassword.setTypeface(appData.getFontRegular());

        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setTypeface(appData.getFontRegular());

        btnRegister.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideSoftKeyboard();

                if(!validated())
                    return;

                goToMainScreen();
            }
        });

        tvTerms = (TextView) findViewById(R.id.tvTerms);
        tvTerms.setTypeface(appData.getFontMedium());

        tvTerms.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        tvPrivacy = (TextView) findViewById(R.id.tvPrivacy);
        tvPrivacy.setTypeface(appData.getFontMedium());

        tvPrivacy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });
    }

    private boolean validated()
    {
        if (!Validator.validateNameNotNull(etUsername.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_username));

            return false;
        }

        if (!Validator.validateNameNotNull(etEmail.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_email));

            return false;
        }

        if (!Validator.validateEmail(etEmail.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_valid_email));

            return false;
        }

        if (!Validator.validateNameNotNull(etPassword.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_password));

            return false;
        }

        return true;
    }
}
