package com.app.swapneo;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.app.swapneo.model.AppData;
import com.app.swapneo.util.LibFile;

public class ActivityBase extends AppCompatActivity
{
    protected AppData appData;
    protected LibFile libFile;

    protected void initData()
    {
        appData = AppData.getInstance(getApplicationContext());

        libFile = LibFile.getInstance(getApplicationContext());
    }

    protected void hideSoftKeyboard()
    {
        if(getCurrentFocus() != null)
        {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    protected void goToLoginScreen()
    {
        Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);

        startActivity(intent);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);

        finish();
    }

    protected void goToSignUpScreen()
    {
        Intent intent = new Intent(getApplicationContext(), ActivitySignup.class);

        startActivity(intent);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);
    }

    protected void goToForgotPasswordScreen()
    {
        Intent intent = new Intent(getApplicationContext(), ActivityForgotPassword.class);

        startActivity(intent);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);
    }

    protected void goToMainScreen()
    {
        Intent intent = new Intent(getApplicationContext(), ActivityMain.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);

        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intent);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);

        finish();
    }

    protected void goToLoginWithClearCache()
    {
        Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intent);

        overridePendingTransition(R.anim.rotate_in, R.anim.rotate_out);

        finish();
    }

    protected void goToSettingScreen()
    {
        Intent intent = new Intent(getApplicationContext(), ActivitySetting.class);

        startActivity(intent);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);
    }

    protected void goToNotificationScreen()
    {
        Intent intent = new Intent(getApplicationContext(), ActivityNotification.class);

        startActivity(intent);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);
    }

    protected void goToRewardsScreen()
    {
        Intent intent = new Intent(getApplicationContext(), ActivityRewards.class);

        startActivity(intent);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);
    }

    protected void goToSwapWifiScreen()
    {
        Intent intent = new Intent(getApplicationContext(), ActivitySwapWifi.class);

        startActivity(intent);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);
    }

    protected void goToProfileScreen()
    {
        Intent intent = new Intent(getApplicationContext(), ActivityProfile.class);

        startActivity(intent);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);
    }

    protected void goToChangePasswordScreen()
    {
        Intent intent = new Intent(getApplicationContext(), ActivityChangePassword.class);

        startActivity(intent);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);
    }

    protected void goToOtherUserProfileScreen()
    {
        Intent intent = new Intent(getApplicationContext(), ActivityOtherUserProfile.class);

        startActivity(intent);

        overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);
    }

    protected void loadWebsite(String website)
    {
        if(!website.contains("http"))
            website = "http://".concat(website);

        if(AppConstants.DEBUG) Log.v(AppConstants.DEBUG_TAG, "URL LINK : "+website);

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(website));
        startActivity(i);
    }

    protected void loadEmail(String email)
    {
        Intent i = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + email));
        //i.setType("message/rfc822");
        //i.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@toogoodtogo.dk"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Contact");
        i.putExtra(Intent.EXTRA_TEXT, "");

        try
        {
            startActivity(Intent.createChooser(i, ""));
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            Toast.makeText(getApplicationContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    protected void goToDirection(String latitude, String longitude)
    {
        String dir = "http://maps.google.com/maps?saddr="+libFile.getLatitude()+","+libFile.getLongitude()+"&daddr="+latitude+","+longitude;

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(dir));
        startActivity(intent);
    }

    protected void loadCall(String number)
    {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:"+ Uri.encode(number.trim())));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
    }

    protected void showSnackBar(View view, String message)
    {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(Color.BLACK);

        TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        tv.setTypeface(appData.getFontRegular());

        snackbar.show();
    }
}
