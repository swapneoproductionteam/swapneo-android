package com.app.swapneo;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

public class ActivitySetting extends ActivityBase
{
    private Switch swNotification, swSound;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_setting);

        initData();

        initUi();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        overridePendingTransition(R.anim.slide_in_from_left_to_right, R.anim.slide_out_from_left_to_right);

        finish();
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.icon_back_left);

        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        ((TextView) findViewById(R.id.tvTitle)).setTypeface(appData.getFontMedium());

        swNotification = (Switch) findViewById(R.id.swNotification);

        swSound = (Switch) findViewById(R.id.swSound);

        ((TextView) findViewById(R.id.tvProfile)).setTypeface(appData.getFontMedium());
        ((TextView) findViewById(R.id.tvMyProfile)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvChangePassword)).setTypeface(appData.getFontRegular());

        ((TextView) findViewById(R.id.tvSetting)).setTypeface(appData.getFontMedium());
        ((TextView) findViewById(R.id.tvNotification)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvSound)).setTypeface(appData.getFontRegular());

        ((TextView) findViewById(R.id.tvAbout)).setTypeface(appData.getFontMedium());
        ((TextView) findViewById(R.id.tvAboutUs)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvTerms)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvPrivacy)).setTypeface(appData.getFontRegular());

        ((TextView) findViewById(R.id.tvLogout)).setTypeface(appData.getFontRegular());

        findViewById(R.id.rlMyProfile).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                goToProfileScreen();
            }
        });

        findViewById(R.id.rlChangePassword).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                goToChangePasswordScreen();
            }
        });

        findViewById(R.id.rlAboutUs).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        findViewById(R.id.rlTerms).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        findViewById(R.id.rlPrivacy).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        findViewById(R.id.rlLogout).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showSignOutAlertMsg(getString(R.string.txt_log_out), getString(R.string.txt_logout_confirmation));
            }
        });
    }

    private void showSignOutAlertMsg(String title,String msg)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        dialogBuilder.setPositiveButton(getString(R.string.txt_yes), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();

                goToLoginWithClearCache();
            }
        });

        dialogBuilder.setNegativeButton(getString(R.string.txt_no), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        dialogBuilder.setTitle(title);

        dialogBuilder.setMessage(msg);

        dialogBuilder.show();
    }
}
