package com.app.swapneo;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.app.swapneo.model.AppData;
import com.app.swapneo.util.LibFile;

import java.text.SimpleDateFormat;

public class FragmentBase extends Fragment
{
    protected View view;

    protected AppData appData;
    protected LibFile libFile;

    protected SimpleDateFormat dateFormat;

    protected void initData()
    {
        appData = AppData.getInstance(getActivity());

        libFile = LibFile.getInstance(getActivity());

        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }

    protected void goToOtherUserProfileScreen()
    {
        Intent intent = new Intent(getActivity(), ActivityOtherUserProfile.class);

        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);
    }

    protected void hideSoftKeyboard()
    {
        if(getActivity().getCurrentFocus() != null)
        {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);

            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    protected void loadWebsite(String website)
    {
        if(!website.contains("http"))
            website = "http://".concat(website);

        if(AppConstants.DEBUG) Log.v(AppConstants.DEBUG_TAG, "URL LINK : "+website);

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(website));
        startActivity(i);
    }

    protected void loadEmail(String email)
    {
        Intent i = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + email));
        //i.setType("message/rfc822");
        //i.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@toogoodtogo.dk"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Contact");
        i.putExtra(Intent.EXTRA_TEXT, "");

        try
        {
            startActivity(Intent.createChooser(i, ""));
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    protected void goToDirection(String latitude, String longitude)
    {
        String dir = "http://maps.google.com/maps?saddr="+libFile.getLatitude()+","+libFile.getLongitude()+"&daddr="+latitude+","+longitude;

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(dir));
        startActivity(intent);
    }

    protected void loadCall(String number)
    {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:"+ Uri.encode(number.trim())));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
    }

    protected void showSnackBar(View view, String message)
    {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(Color.BLACK);

        TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        tv.setTypeface(appData.getFontRegular());

        snackbar.show();
    }
}
