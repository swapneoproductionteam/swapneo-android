package com.app.swapneo;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.github.siyamed.shapeimageview.RoundedImageView;

public class ActivityOtherUserProfile extends ActivityBase
{
    private Toolbar toolbar;

    private CircularImageView ivImage;

    private Button btnMessage, btnFollow;
    private TextView tvFollowers, tvFollowing;

    private RecyclerView rvPhotos;

    private AdapterPhotos adpPhotos;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_other_user_profile);

        initData();

        initUi();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        overridePendingTransition(R.anim.slide_in_from_left_to_right, R.anim.slide_out_from_left_to_right);

        finish();
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.icon_back_left);

        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        ((TextView) findViewById(R.id.tvTitle)).setTypeface(appData.getFontMedium());

        ivImage = (CircularImageView) findViewById(R.id.ivImage);

        btnMessage = (Button) findViewById(R.id.btnMessage);
        btnMessage.setTypeface(appData.getFontRegular());

        btnFollow = (Button) findViewById(R.id.btnFollow);
        btnFollow.setTypeface(appData.getFontRegular());

        tvFollowers = (TextView) findViewById(R.id.tvFollowers);
        tvFollowers.setTypeface(appData.getFontRegular());

        tvFollowing = (TextView) findViewById(R.id.tvFollowing);
        tvFollowing.setTypeface(appData.getFontRegular());

        rvPhotos = (RecyclerView) findViewById(R.id.rvPhotos);
        rvPhotos.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));

        adpPhotos = new AdapterPhotos();
        rvPhotos.setAdapter(adpPhotos);
    }

    public class AdapterPhotos extends RecyclerView.Adapter<AdapterPhotos.MyViewHolder>
    {
        public class MyViewHolder extends RecyclerView.ViewHolder
        {
            private RoundedImageView ivImage;

            public MyViewHolder(View view)
            {
                super(view);

                ivImage = (RoundedImageView) view.findViewById(R.id.ivImage);
            }
        }

        public AdapterPhotos()
        {
            //this.moviesList = moviesList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photos, parent, false);

            //itemView.setOnClickListener(mOnClickListener);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position)
        {
            /*if(alImages.get(position).getImage().equals("ADD_NEW_IMAGE"))
                holder.ivImage.setImageDrawable(getResources().getDrawable(R.drawable.icon_add_image));
            else
            {
                Picasso.with(getApplicationContext()).load(alImages.get(position).getImage()).placeholder(R.drawable.icon_no_image).into(holder.ivImage);
            }*/
        }

        @Override
        public int getItemCount()
        {
            return 50;
        }
    }
}
