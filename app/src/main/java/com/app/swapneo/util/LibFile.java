package com.app.swapneo.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.swapneo.AppConstants;

public class LibFile
{
    Context context;

    SharedPreferences settings;

    private static LibFile instance;

    public static LibFile getInstance(Context context)
    {
        if(instance == null)
        {
            instance = new LibFile(context);

        }
        return instance;
    }

    private LibFile(Context context)
    {
        this.context  = context;

        settings = context.getSharedPreferences(AppConstants.PREFS_FILE_NAME_PARAM , 0);
    }

    public String getLatitude()
    {
        return settings.getString("latitude", "");
    }

    public void setLatitude(String latitude)
    {
        settings.edit().putString("latitude", latitude).commit();
    }

    public String getLongitude()
    {
        return settings.getString("longitude", "");
    }

    public void setLongitude(String longitude)
    {
        settings.edit().putString("longitude", longitude).commit();
    }

}
