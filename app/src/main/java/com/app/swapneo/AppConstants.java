package com.app.swapneo;

public class AppConstants
{
    public static final boolean DEBUG = true;

    public static final String DEBUG_TAG = "SWAP_NEO";

    public static final String PREFS_FILE_NAME_PARAM = "SWAPNEO.PREF";

    public static final int LOAD_SETTINGS = 1;
    public static final int LOAD_NOTIFICATION = 2;
    public static final int LOAD_REWARDS = 3;
    public static final int LOAD_SWAP_WIFI = 4;
    public static final int LOAD_INVITE = 5;
    public static final int LOAD_LOGOUT = 6;
    public static final int LOAD_ABOUT = 7;
    public static final int LOAD_PRIVACY = 8;
    public static final int LOAD_TERMS = 9;
}
