package com.app.swapneo;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.ss.bottomnavigation.BottomNavigation;
import com.ss.bottomnavigation.events.OnSelectedItemChangeListener;

import nl.psdcompany.duonavigationdrawer.views.DuoDrawerLayout;
import nl.psdcompany.duonavigationdrawer.views.DuoMenuView;
import nl.psdcompany.duonavigationdrawer.widgets.DuoDrawerToggle;


public class ActivityMain extends ActivityBase
{
    private int lastSelectedPosition = 0;

    private Toolbar toolbar;

    private DuoDrawerLayout mDuoDrawerLayout;
    private DuoMenuView mDuoMenuView;

    private BottomNavigation bottomNavigation;

    private TextView tvName;
    private CircularImageView ivImage;

    private ViewPager viewpager;

    private boolean isExit;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initData();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDuoDrawerLayout = (DuoDrawerLayout) findViewById(R.id.drawer);
        mDuoMenuView = (DuoMenuView) mDuoDrawerLayout.getMenuView();

        DuoDrawerToggle duoDrawerToggle = new DuoDrawerToggle(this,
                mDuoDrawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        mDuoDrawerLayout.setDrawerListener(duoDrawerToggle);
        duoDrawerToggle.syncState();

        initUi();
    }

    @Override
    public void onBackPressed()
    {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0)
        {
            getSupportFragmentManager().popBackStack();
        }
        else
        {
            /*if (drawer.isDrawerOpen(GravityCompat.START))
            {
                drawer.closeDrawer(GravityCompat.START);
            }
            else*/
            {
                if(isExit)
                {
                    super.onBackPressed();
                }
                else
                {
                    isExit = true;

                    Toast.makeText(ActivityMain.this, "Press back again to exit", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        bottomNavigation=(BottomNavigation)findViewById(R.id.bottomNavigation);
        bottomNavigation.setTypeface(appData.getFontRegular());

        viewpager = (ViewPager) findViewById(R.id.viewpager);
        PagerAdapter adapter = new PagerAdapter (getSupportFragmentManager());
        viewpager.setAdapter(adapter);

        bottomNavigation.setOnSelectedItemChangeListener(new OnSelectedItemChangeListener() {
            @Override
            public void onSelectedItemChanged(int itemId)
            {
                switch (itemId)
                {
                    case R.id.tab_home:
                        viewpager.setCurrentItem(0);
                        //Toast.makeText(ActivityMain.this, "HOME", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.tab_chat:
                        viewpager.setCurrentItem(1);
                        //Toast.makeText(ActivityMain.this, "CHAT", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.tab_upload:
                        viewpager.setCurrentItem(2);
                        //Toast.makeText(ActivityMain.this, "UPLOAD", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.tab_photos:
                        viewpager.setCurrentItem(3);
                        //Toast.makeText(ActivityMain.this, "PHOTOS", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });

        tvName = (TextView) findViewById(R.id.tvName);
        tvName.setTypeface(appData.getFontRegular());

        ivImage = (CircularImageView) findViewById(R.id.ivImage);

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                bottomNavigation.setSelectedItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });

        ((TextView) findViewById(R.id.tvSettings)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvNotification)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvRewards)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvSwapWifi)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvInvite)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvLogout)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvAbout)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvPrivacy)).setTypeface(appData.getFontRegular());
        ((TextView) findViewById(R.id.tvTerms)).setTypeface(appData.getFontRegular());

        findViewById(R.id.llSettings).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loadDrawerItems(AppConstants.LOAD_SETTINGS);
            }
        });

        findViewById(R.id.llNotification).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loadDrawerItems(AppConstants.LOAD_NOTIFICATION);
            }
        });

        findViewById(R.id.llRewards).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loadDrawerItems(AppConstants.LOAD_REWARDS);
            }
        });

        findViewById(R.id.llSwapWifi).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loadDrawerItems(AppConstants.LOAD_SWAP_WIFI);
            }
        });

        findViewById(R.id.llInvite).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loadDrawerItems(AppConstants.LOAD_INVITE);
            }
        });

        findViewById(R.id.llLogout).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loadDrawerItems(AppConstants.LOAD_LOGOUT);
            }
        });

        findViewById(R.id.llAbout).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loadDrawerItems(AppConstants.LOAD_ABOUT);
            }
        });

        findViewById(R.id.llPrivacy).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loadDrawerItems(AppConstants.LOAD_PRIVACY);
            }
        });

        findViewById(R.id.llTerms).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loadDrawerItems(AppConstants.LOAD_TERMS);
            }
        });
    }

    public class PagerAdapter extends FragmentStatePagerAdapter
    {
        public PagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position)
            {
                case 0:
                    FragmentHome tab1 = FragmentHome.newInstance();
                    return tab1;
                case 1:
                    FragmentChat tab2 = FragmentChat.newInstance();
                    return tab2;

                case 2:
                    FragmentUpload tab3 = FragmentUpload.newInstance();
                    return tab3;

                case 3:
                    FragmentPhotos tab4 = FragmentPhotos.newInstance();
                    return tab4;

                default:
                    return null;
            }
        }

        @Override
        public int getCount()
        {
            return 4;
        }
    }

    private void loadDrawerItems(int index)
    {
        switch (index)
        {
            case AppConstants.LOAD_SETTINGS:
                goToSettingScreen();
                break;

            case AppConstants.LOAD_NOTIFICATION:
                goToNotificationScreen();
                break;

            case AppConstants.LOAD_REWARDS:
                goToRewardsScreen();
                break;

            case AppConstants.LOAD_SWAP_WIFI:
                goToSwapWifiScreen();
                break;

            case AppConstants.LOAD_INVITE:
                break;

            case AppConstants.LOAD_LOGOUT:
                showSignOutAlertMsg(getString(R.string.txt_log_out), getString(R.string.txt_logout_confirmation));
                break;

            case AppConstants.LOAD_ABOUT:
                break;

            case AppConstants.LOAD_PRIVACY:
                break;

            case AppConstants.LOAD_TERMS:
                break;
        }
        mDuoDrawerLayout.closeDrawer();
        //drawer.closeDrawers();
    }

    private void showSignOutAlertMsg(String title,String msg)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        dialogBuilder.setPositiveButton(getString(R.string.txt_yes), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();

                goToLoginWithClearCache();
            }
        });

        dialogBuilder.setNegativeButton(getString(R.string.txt_no), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        dialogBuilder.setTitle(title);

        dialogBuilder.setMessage(msg);

        dialogBuilder.show();
    }
}
