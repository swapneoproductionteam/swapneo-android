package com.app.swapneo;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.siyamed.shapeimageview.RoundedImageView;

public class FragmentPhotos extends FragmentBase
{
    private RecyclerView rvPhotos;

    private AdapterPhotos adpPhotos;

    public static FragmentPhotos newInstance()
    {
        FragmentPhotos fragment = new FragmentPhotos();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_photos, null);

        initData();

        initUi();

        return view;
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        rvPhotos = (RecyclerView) view.findViewById(R.id.rvPhotos);
        rvPhotos.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        adpPhotos = new AdapterPhotos();
        rvPhotos.setAdapter(adpPhotos);
    }

    public class AdapterPhotos extends RecyclerView.Adapter<AdapterPhotos.MyViewHolder>
    {
        public class MyViewHolder extends RecyclerView.ViewHolder
        {
            private RoundedImageView ivImage;

            public MyViewHolder(View view)
            {
                super(view);

                ivImage = (RoundedImageView) view.findViewById(R.id.ivImage);
            }
        }

        public AdapterPhotos()
        {
            //this.moviesList = moviesList;
        }

        @Override
        public AdapterPhotos.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photos, parent, false);

            //itemView.setOnClickListener(mOnClickListener);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position)
        {
            /*if(alImages.get(position).getImage().equals("ADD_NEW_IMAGE"))
                holder.ivImage.setImageDrawable(getResources().getDrawable(R.drawable.icon_add_image));
            else
            {
                Picasso.with(getApplicationContext()).load(alImages.get(position).getImage()).placeholder(R.drawable.icon_no_image).into(holder.ivImage);
            }*/
        }

        @Override
        public int getItemCount()
        {
            return 50;
        }
    }
}
