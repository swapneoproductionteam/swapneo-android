package com.app.swapneo;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.swapneo.util.Validator;

public class ActivityLogin extends ActivityBase
{
    private EditText etEmail, etPassword;

    private TextView tvForgotPass;

    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        initData();

        initUi();
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        ((TextView) findViewById(R.id.tvLogin)).setTypeface(appData.getFontMedium());

        //((TextView) findViewById(R.id.tvNoAcct)).setTypeface(appData.getFontRegular());

        ((TextInputLayout) findViewById(R.id.tlEmail)).setTypeface(appData.getFontRegular());
        ((TextInputLayout) findViewById(R.id.tlPassword)).setTypeface(appData.getFontRegular());

        etEmail = (EditText) findViewById(R.id.etEmail);
        etEmail.setTypeface(appData.getFontRegular());

        etPassword = (EditText) findViewById(R.id.etPassword);
        etPassword.setTypeface(appData.getFontRegular());

        tvForgotPass = (TextView) findViewById(R.id.tvForgotPass);
        tvForgotPass.setTypeface(appData.getFontRegular());

        tvForgotPass.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                goToForgotPasswordScreen();
            }
        });

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setTypeface(appData.getFontRegular());

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                if(!validated())
                    return;

                goToMainScreen();
            }
        });
    }

    private boolean validated()
    {
        if (!Validator.validateNameNotNull(etEmail.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_email));

            return false;
        }

        if (!Validator.validateEmail(etEmail.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_valid_email));

            return false;
        }

        if (!Validator.validateNameNotNull(etPassword.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_password));

            return false;
        }

        return true;
    }
}
