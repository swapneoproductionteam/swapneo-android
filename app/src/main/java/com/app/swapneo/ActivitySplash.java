package com.app.swapneo;

import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

public class ActivitySplash extends ActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        initData();

        initUi();
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        new Timer().schedule(new TimerTask()
        {
            @Override
            public void run()
            {
               runOnUiThread(new Runnable()
               {
                   @Override
                   public void run()
                   {
                       goToLoginScreen();
                   }
               });
            }
        }, 2000);
    }
}
