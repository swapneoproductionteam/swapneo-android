package com.app.swapneo;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;

public class FragmentHome extends FragmentBase
{
    private SwipeRefreshLayout swipeLayout;

    private RecyclerView rvPosts;

    private AdapterPosts adpPosts;

    public static FragmentHome newInstance()
    {
        FragmentHome fragment = new FragmentHome();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_home, null);

        initData();

        initUi();

        swipeLayout.post(new Runnable()
        {
            @Override
            public void run()
            {
                //swipeLayout.setRefreshing(true);
            }
        });

        return view;
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeLayout);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {

            }
        });

        rvPosts = (RecyclerView) view.findViewById(R.id.rvPosts);
        rvPosts.setLayoutManager(new LinearLayoutManager(getActivity()));

        adpPosts = new AdapterPosts();
        rvPosts.setAdapter(adpPosts);
    }

    public class AdapterPosts extends RecyclerView.Adapter<AdapterPosts.MyViewHolder>
    {
        public class MyViewHolder extends RecyclerView.ViewHolder
        {
            private CircularImageView ivImage;

            private ImageView ivMainImage;

            private TextView tvName;

            private RelativeLayout llWhereTo;

            public MyViewHolder(View view)
            {
                super(view);

                tvName = (TextView) view.findViewById(R.id.tvName);
                tvName.setTypeface(appData.getFontRegular());

                ivMainImage = (ImageView) view.findViewById(R.id.ivMainImage);

                ivImage = (CircularImageView) view.findViewById(R.id.ivImage);

                llWhereTo = (RelativeLayout) view.findViewById(R.id.llWhereTo);
            }
        }

        public AdapterPosts()
        {
            //this.moviesList = moviesList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_posts, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position)
        {
            holder.llWhereTo.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    goToOtherUserProfileScreen();
                }
            });

            holder.tvName.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    goToOtherUserProfileScreen();
                }
            });

            holder.ivImage.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    goToOtherUserProfileScreen();
                }
            });
        }

        @Override
        public int getItemCount()
        {
            return 15;
        }
    }
}
