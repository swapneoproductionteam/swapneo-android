package com.app.swapneo;


import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.swapneo.util.Validator;

public class ActivityChangePassword extends ActivityBase
{
    private Toolbar toolbar;

    private EditText etPassword, etRePassword;

    private Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_change_password);

        initData();

        initUi();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        overridePendingTransition(R.anim.slide_in_from_left_to_right, R.anim.slide_out_from_left_to_right);

        finish();
    }

    protected void initData()
    {
        super.initData();
    }

    private void initUi()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.icon_back_left);

        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        ((TextView) findViewById(R.id.tvTitle)).setTypeface(appData.getFontMedium());

        etPassword = (EditText) findViewById(R.id.etPassword);
        etPassword.setTypeface(appData.getFontRegular());

        etRePassword = (EditText) findViewById(R.id.etRePassword);
        etRePassword.setTypeface(appData.getFontRegular());

        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnUpdate.setTypeface(appData.getFontRegular());

        btnUpdate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideSoftKeyboard();

                if(!validated())
                    return;

                onBackPressed();
            }
        });
    }

    private boolean validated()
    {
        if (!Validator.validateNameNotNull(etPassword.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_password));

            return false;
        }

        if (!Validator.validateNameNotNull(etRePassword.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_confirm_password));

            return false;
        }

        if(!etPassword.getText().toString().equals(etRePassword.getText().toString()))
        {
            showSnackBar(findViewById(R.id.rlMain), getString(R.string.txt_err_pass_match));

            return false;
        }

        return true;
    }
}
